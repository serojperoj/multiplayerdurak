<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login page</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/my.js"></script>
</head>
<body>
<s:form action="login" method="get">
<s:textfield name="userName" label="Enter your name" />
<s:submit label="Enter game" />
</s:form>
</body>
</html>