$(function() {

	$("#introForm").click(function() {

		// var formInput=$(this).serialize();
		$.getJSON('welcome.action', function(data) {

			$('#player1div').html('');
			$.each(data.gameService.game.player1.cardsOnHand, function(index) {
				$('#player1div').append("<img src=" + this.path + " id=" + this.path + " />");
				
			});
			$('#player2div').html('');
			$.each(data.gameService.game.player2.cardsOnHand, function(index) {
				$('#player2div').append("<img src=" + this.path + " id=" + this.path + " />");
			});
			$('#dealdiv').html('');
			$.each(data.gameService.game.lobby, function(index) {
				$('#dealdiv').append('<img src=' + this.path + ' />');
			});

		});

		return false;

	});

	$("#player1div").on("click", "img", function() {
		var id = $(this).attr('id');
		$.getJSON('welcome.action', {
			command : "player1",
			id : id
		}, function(data) {

			$('#player1div').html('');
			$.each(data.gameService.game.player1.cardsOnHand, function(index) {
				$('#player1div').append("<img src=" + this.path + " id=" + this.path + " />");
				
			});

			$('#dealdiv').html('');
			$.each(data.gameService.game.lobby, function(index) {
				$('#dealdiv').append('<img src=' + this.path + ' />');
			});

		});
	});

	$("#player2div").on("click", "img", function() {
		var id = $(this).attr('id');
		$.getJSON('welcome.action', {
			command : "player2",
			id : id
		}, function(data) {

			$('#player2div').html('');
			$.each(data.gameService.game.player2.cardsOnHand, function(index) {
				$('#player2div').append("<img src=" + this.path + " id=" + this.path + " />");
			});

			$('#dealdiv').html('');
			$.each(data.gameService.game.lobby, function(index) {
				$('#dealdiv').append('<img src=' + this.path + ' />');
			});

		});
	});

	$("#bitta").click(function() {
		$.getJSON('welcome.action', {command : "bitta"}, function(data) {
			$('#player1div').html('');
			$.each(data.gameService.game.player1.cardsOnHand, function(index) {
				$('#player1div').append("<img src=" + this.path + " id=" + this.path + " />");
				
			});
			$('#player2div').html('');
			$.each(data.gameService.game.player2.cardsOnHand, function(index) {
				$('#player2div').append("<img src=" + this.path + " id=" + this.path + " />");
			});
			$('#dealdiv').html('');
			$.each(data.gameService.game.lobby, function(index) {
				$('#dealdiv').append('<img src=' + this.path + ' />');
			});
		});
	});
	
	$("#havaqel").click(function() {
		$.getJSON('welcome.action', {command : "havaqel"}, function(data) {
			$('#player1div').html('');
			$.each(data.gameService.game.player1.cardsOnHand, function(index) {
				$('#player1div').append("<img src=" + this.path + " id=" + this.path + " />");
				
			});
			$('#player2div').html('');
			$.each(data.gameService.game.player2.cardsOnHand, function(index) {
				$('#player2div').append("<img src=" + this.path + " id=" + this.path + " />");
			});
			$('#dealdiv').html('');
			$.each(data.gameService.game.lobby, function(index) {
				$('#dealdiv').append('<img src=' + this.path + ' />');
			});
		});
	});
	
});