function initialize() {
	refreshCards();
	initializeChat();
}

function update(data) {
	
	$('#kalodadiv').html('');
	if (data.packSize > 0) {
		$('#kalodadiv').append('<img src=' + data.kozr.path + ' class="kozr" />');
	}

	var img = "<img src='img/cover.gif' class='cover' />";
	var x = 0;
	for (var i=0; i <= data.packSize-1; i++) {
		
		if (x < 16) {
			$('#kalodadiv').append(img);
			$('#kalodadiv .cover').last().css('left', (x+20));
			x = x + 2;
		}
		
	}
	
	$('#player2div').html('');
	var y = 0;
	$.each(data.player.cardsOnHand, function(index) {
		$('#player2div').append("<img src=" + this.path + " id=" + this.path + " />");

		$('#player2div img').last().css({
			'position' : 'absolute',
			'top' : '0px',
			'left' : y+"px"
			}).hover(
				function () {
					$(this).css({'top' : '-10px'});
					},
				function () {
					$(this).css({'top' : '0px'});
				});
		y = y + 20;	
	});
	
	$('#player1div').html('');
	if (data.opponentCardCount > 0) {
		var y = 0;
		var img = "<img src='img/cover.gif' />";
		for (var i=0; i < data.opponentCardCount; i++) {
			$('#player1div').append(img);
			
			$('#player1div img').last().css({
				'position' : 'absolute',
				'top' : '0px',
				'left' : y+"px"
				});
			y = y + 20;
		}
	}
	
	$('#dealdiv1').html('');
	var x = 0;
	var y = 0;
	var j = 0;
	$.each(data.lobby, function(index) {
		$('#dealdiv1').append('<img src=' + this.path + ' />');
		$('#dealdiv1 img').last().css({
			'position' : 'absolute',
			'top' : '0px',
			'left' : y + 'px'
			});
		
		if (j % 2 == 0) {
			y = y + 20;
		} else {
			y = y + 80;
		}
		j++;
		
	});
	
	if (data.player.message != null) {
		$('#messagediv').html('<p><font color=white><b>'+ data.player.message + '</b></font></p>');
	} else {
		$('#messagediv').html('');
	}
	
	if (data.player.win == "win") {
		alert("You Win");
		redirect();
	} else if (data.player.win == "loss") {
		alert("You Loss");
		redirect();
	} else if (data.player.win == "nichya") {
		alert("Nichya!");
		redirect();
	}

}

function refreshCards() {
	var gameId = $('input[name=gameId]').attr('id');
	$.getJSON('gameJSON.action', { gameId : gameId }, function(data) {
		update(data);
	});
	setTimeout(refreshCards, 3000);
}

function redirect() {
	var userName = $('input[name=userName]').attr('id');
	$(window.location).attr('href', 'login.action?userName='+userName);
}

$(function() {

	$("#player2div").on("click","img",function() {
		
		var id = $(this).attr('id');
		var gameId = $('input[name=gameId]').attr('id');
		
		$.getJSON('gameJSON.action', {
			command : "move",
			id : id,
			gameId : gameId
		}, function(data) {
			
			update(data);

		});

	});
	
	$("#bitta").click(function() {
		var gameId = $('input[name=gameId]').attr('id');
		
		$.getJSON('gameJSON.action', {
			command : "bitta",
			gameId : gameId
		}, function(data) {
			update(data);
		});
	});
	
	$("#take").click(function() {
		var gameId = $('input[name=gameId]').attr('id');
		
		$.getJSON('gameJSON.action', {
			command : "take",
			gameId : gameId
		}, function(data) {
			update(data);
		});
	});
	
	$("#sitdown").click(function() {
		var gameId = $('input[name=gameId]').attr('id');
		
		$.getJSON('gameJSON.action', {
			command : "sitdown",
			gameId : gameId
		}, function(data) {
			update(data);
		});
	});

});