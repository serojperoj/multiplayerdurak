var messageIndex = 0;
function initializeChat() {
	setupEnterKeyEvent();
	setInputFocus();
	getNewMessages();
}
function setupEnterKeyEvent() {
	$("#chatInput").keyup(function(e) {
		if (e.keyCode == 13) {
			submitChatInput();
		}
	});
}

function setInputFocus() {
	$("#chatInput").focus();
}

function submitChatInput() {
	var chatInputJquery = $("#chatInput");
	var messageText = chatInputJquery.val();
	chatInputJquery.val('');
	sendMessage(messageText);
	setInputFocus();
}

function sendMessage(messageText) {
//	alert(messageIndex);
	$.getJSON("chatJSON.action", {
		command : 'addmessage',
		singleMessage : messageText,
		index : messageIndex
		}, function(data){
			updateChatOutput(data.messages);
//			messageIndex = data.messages.length;
		});
	
}

function updateChatOutput(messages) {
//	var chatInputJquery = $("#chatInput");
	var chatOutputJquery = $("#chatOutput");
	$.each(messages, function(index) {
		var outputText = "<b> " + this[0] + " </b> " + " : " + this[1] + " <br/>";	
		chatOutputJquery.append(outputText);
	});
	messageIndex = messageIndex + messages.length;
	chatOutputJquery.scrollTop(chatOutputJquery.prop('scrollHeight'));
}

function getNewMessages() {
//	alert(messageIndex);
	$.getJSON("chatJSON.action", {
		command : 'getmessages',
		index : messageIndex
	}, function(data){
			if (data.messages) {
				updateChatOutput(data.messages);
			}
//			messageIndex = data.messages.length;
			setTimeout(getNewMessages, 3000);
  		});
}