<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/lobby.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/lobby.js"></script>
<script type="text/javascript" src="js/chat.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			initialize();
		});
</script>
</head>
<body>
<div id="maindiv">
<H1>Hello <s:property value="%{#session.userName}"/></H1>
<table id="mainTable" cellpadding="25" cellspacing="25" border="1">
<tr>
<s:iterator value="gameContainer" var="games" status="status">
<td align="center">
<%-- <h5>Table <s:property value="#status.index"/></h5> --%>
<img src="img/title.jpg" /><br/>
<%-- <a href="game.action?gameId=<s:property value="#status.index"/>&playerId=first" id="first<s:property value="#status.index"/>" style="display: none;">Player 1</a> --%>
<%-- <a href="game.action?gameId=<s:property value="#status.index"/>&playerId=last" id="last<s:property value="#status.index"/>" style="display: none;">Player 2</a> --%>
<a href="game.action?gameId=<s:property value="#status.index"/>" id="game<s:property value="#status.index"/>" style="display: none;">Play</a>
</td>
</s:iterator>
</tr>
</table>
<p>Chat</p>
<div id="chatOutput"></div>
<input type="text" id="chatInput"></input>
</div>
<div id="result"></div>


</body>
</html>