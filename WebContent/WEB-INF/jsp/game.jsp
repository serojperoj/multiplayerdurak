<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jQueryRotate.js"></script>
<script type="text/javascript" src="js/game.js"></script>
<script type="text/javascript" src="js/chat.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			initialize();
		});
</script>
</head>
<body>
<!-- <div id="maindiv" style="background-image: url(img/bg.jpg)"> -->
<div id="maindiv">
	<div id="player1div">
	</div>
	<div id="player2div">
	</div>
	<div id="dealdiv1">
	</div>
	<div id="dealdiv2">
	</div>
	<div id="kalodadiv">
	</div>
	<div id="messagediv">
	</div>
</div>
<input id="sitdown" type="button" value="Sit Down" />
<!-- <input id="standup" type="button" value="Stand Up" /> -->
<input id="bitta" type="button" value="Bitta" />
<input id="take" type="button" value="Take" />
<div id="chatOutput"></div>
<input type="text" id="chatInput"></input>

<input type="hidden" id="<s:property value="%{#session.userName}"/>" name="userName"/>
<input type="hidden" id="<s:property value="gameId"/>" name="gameId"/>
<h1>Hello <s:property value="%{#session.userName}"/></h1>
</body>
</html>