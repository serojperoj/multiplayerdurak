package am.seroj.model;

public class Card {
	
	private String mast;
	private int value;
	private String path;
	
	public Card(String mast, int value, String path) {
		super();
		this.setMast(mast);
		this.setValue(value);
		this.setPath(path);
	}

	public String getMast() {
		return mast;
	}

	public void setMast(String mast) {
		this.mast = mast;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
