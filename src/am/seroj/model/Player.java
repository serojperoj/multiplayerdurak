package am.seroj.model;

import java.util.LinkedList;

public class Player {
	private String name;
	private Boolean turn = false;
	private Boolean ready = false;
	private LinkedList<Card> cardsOnHand;
	private String win = "";
	private String message;
	
	public Player() {
		cardsOnHand = new LinkedList<Card>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getTurn() {
		return turn;
	}

	public void setTurn(Boolean turn) {
		this.turn = turn;
	}

	public LinkedList<Card> getCardsOnHand() {
		return cardsOnHand;
	}

	public void setCardsOnHand(LinkedList<Card> cardsOnHand) {
		this.cardsOnHand = cardsOnHand;
	}

	public Boolean getReady() {
		return ready;
	}

	public void setReady(Boolean ready) {
		this.ready = ready;
	}

	public String getWin() {
		return win;
	}

	public void setWin(String win) {
		this.win = win;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
