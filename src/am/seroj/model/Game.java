package am.seroj.model;

import java.util.LinkedList;

public class Game {
	
	private LinkedList<Player> player;
	private CardPack cardPack;
	private LinkedList<Card> lobby;
	private Boolean status = false;
	private Boolean giveMoreVariable = false;;
	private int id;
	
	public Game() {
		cardPack = new CardPack();
		lobby = new LinkedList<>();
		player = new LinkedList<>();
		player.add(new Player());
		player.add(new Player());
	}

	public CardPack getCardPack() {
		return cardPack;
	}
	public void setCardPack(CardPack cardPack) {
		this.cardPack = cardPack;
	}
	public LinkedList<Card> getLobby() {
		return lobby;
	}
	public void setLobby(LinkedList<Card> lobby) {
		this.lobby = lobby;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LinkedList<Player> getPlayer() {
		return player;
	}

	public void setPlayer(LinkedList<Player> player) {
		this.player = player;
	}

	public Boolean getGiveMoreVariable() {
		return giveMoreVariable;
	}

	public void setGiveMoreVariable(Boolean giveMoreVariable) {
		this.giveMoreVariable = giveMoreVariable;
	}
}
