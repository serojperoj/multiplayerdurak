package am.seroj.model;

import java.util.LinkedList;

public class CardPack {
	
	private LinkedList<Card> pack;
	
	private Card kozr;
	
	public CardPack() {
		
		setPack(new LinkedList<Card>());
		
		getPack().add(new Card("xach", 6, "img/card26.gif"));
		getPack().add(new Card("xach", 7, "img/card27.gif"));
		getPack().add(new Card("xach", 8, "img/card28.gif"));
		getPack().add(new Card("xach", 9, "img/card29.gif"));
		getPack().add(new Card("xach", 10, "img/card210.gif"));
		getPack().add(new Card("xach", 11, "img/card211.gif"));
		getPack().add(new Card("xach", 12, "img/card212.gif"));
		getPack().add(new Card("xach", 13, "img/card213.gif"));
		getPack().add(new Card("xach", 14, "img/card214.gif"));
		getPack().add(new Card("qyap", 6, "img/card16.gif"));
		getPack().add(new Card("qyap", 7, "img/card17.gif"));
		getPack().add(new Card("qyap", 8, "img/card18.gif"));
		getPack().add(new Card("qyap", 9, "img/card19.gif"));
		getPack().add(new Card("qyap", 10, "img/card110.gif"));
		getPack().add(new Card("qyap", 11, "img/card111.gif"));
		getPack().add(new Card("qyap", 12, "img/card112.gif"));
		getPack().add(new Card("qyap", 13, "img/card113.gif"));
		getPack().add(new Card("qyap", 14, "img/card114.gif"));
		getPack().add(new Card("ghar", 6, "img/card36.gif"));
		getPack().add(new Card("ghar", 7, "img/card37.gif"));
		getPack().add(new Card("ghar", 8, "img/card38.gif"));
		getPack().add(new Card("ghar", 9, "img/card39.gif"));
		getPack().add(new Card("ghar", 10, "img/card310.gif"));
		getPack().add(new Card("ghar", 11, "img/card311.gif"));
		getPack().add(new Card("ghar", 12, "img/card312.gif"));
		getPack().add(new Card("ghar", 13, "img/card313.gif"));
		getPack().add(new Card("ghar", 14, "img/card314.gif"));
		getPack().add(new Card("sirt", 6, "img/card06.gif"));
		getPack().add(new Card("sirt", 7, "img/card07.gif"));
		getPack().add(new Card("sirt", 8, "img/card08.gif"));
		getPack().add(new Card("sirt", 9, "img/card09.gif"));
		getPack().add(new Card("sirt", 10, "img/card010.gif"));
		getPack().add(new Card("sirt", 11, "img/card011.gif"));
		getPack().add(new Card("sirt", 12, "img/card012.gif"));
		getPack().add(new Card("sirt", 13, "img/card013.gif"));
		getPack().add(new Card("sirt", 14, "img/card014.gif"));	
	}
	
	public LinkedList<Card> getPack() {
		return pack;
	}

	public void setPack(LinkedList<Card> pack) {
		this.pack = pack;
	}

	public Card getKozr() {
		return kozr;
	}

	public void setKozr(Card kozr) {
		this.kozr = kozr;
	}
}
