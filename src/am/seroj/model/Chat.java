package am.seroj.model;

import java.util.LinkedList;
import java.util.List;

public class Chat {
	
	private List<String[]> messages;
	
	public Chat() {
		setMessages(new LinkedList<String[]>());
	}

	public List<String[]> getMessages() {
		return messages;
	}

	public void setMessages(List<String[]> messages) {
		this.messages = messages;
	}

}
