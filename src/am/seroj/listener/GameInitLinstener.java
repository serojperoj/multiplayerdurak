package am.seroj.listener;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import am.seroj.model.Game;
import am.seroj.service.ChatService;

public class GameInitLinstener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		
		ServletContext servletContext = event.getServletContext();
		
		List<Game> gameContainer = new ArrayList<Game>();
		gameContainer.add(new Game());
		gameContainer.add(new Game());
		gameContainer.add(new Game());

		servletContext.setAttribute("gameContainer", gameContainer);
		System.out.println("contextInitialized");
		
		ChatService chatService = new ChatService();
		servletContext.setAttribute("chatService", chatService);
		
		System.out.println("chat service initialized");
	}

}
