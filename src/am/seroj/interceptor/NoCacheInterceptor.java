package am.seroj.interceptor;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.StrutsStatics;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class NoCacheInterceptor implements Interceptor, ServletResponseAware {

	private static final long serialVersionUID = 1L;
	private HttpServletResponse response;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		ActionContext context = actionInvocation.getInvocationContext();
		HttpServletResponse resposne = (HttpServletResponse) context.get(StrutsStatics.HTTP_RESPONSE);
		if (response != null) {
			System.out.println("not null");
			response.setHeader("Cache-control","no-cache,no-store");
	        response.setHeader("Pragma","no-cache");
	        response.setHeader("Expires","-1");
		}
		System.out.println("null");
		return actionInvocation.invoke();
	}

	@Override
	public void setServletResponse(HttpServletResponse resposne) {
		this.response = response;		
	}

}
