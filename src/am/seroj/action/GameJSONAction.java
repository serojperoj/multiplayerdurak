package am.seroj.action;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;

import am.seroj.model.Card;
import am.seroj.model.Game;
import am.seroj.model.Player;
import am.seroj.service.GameService;

import com.opensymphony.xwork2.Action;

public class GameJSONAction implements Action, SessionAware, ApplicationAware {

	private Map<String, Object> application;
	private Map<String, Object> session;
	private Player player;
	private GameService gameService;
	private Game game;
	private String gameId;
	private Card kozr;
	private LinkedList<Card> lobby;
	private int packSize;
	private String command;
	private String id;
	private int opponentCardCount;

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public String execute() throws Exception {
		if (getGameId() != null) {
			List<Game> gameContainer = (List<Game>) application
					.get("gameContainer");
			game = gameContainer.get(Integer.parseInt(getGameId()));
			
			if (session.containsKey("player")) {
				player = (Player) session.get("player");
			}
			if (getCommand() != null) {
				gameService = new GameService(game);
				if (getCommand().equals("sitdown")) {
					if (game.getPlayer().getFirst().getReady() == false) {
						player = game.getPlayer().getFirst();
						player.setReady(true);
						session.put("player", player);
					} else if (game.getPlayer().getLast().getReady() == false) {
						player = game.getPlayer().getLast();
						player.setReady(true);
						session.put("player", player);
						gameService.startGame();
						gameService.getGame().getPlayer().getFirst().setTurn(true);
					}
				}
				if (getCommand().equals("move")) {
					gameService.move(player, gameService.findAndGetCardById(player, getId()));
				}
				if (getCommand().equals("bitta")) {
					gameService.bitta(player);
				}
				if (getCommand().equals("take")) {
					gameService.take(player);
				}

			}
			if (game.getStatus()) {
				setKozr(game.getCardPack().getKozr());
				setLobby(game.getLobby());
				setPackSize(game.getCardPack().getPack().size());
				if (player == game.getPlayer().getFirst()) {
					setOpponentCardCount(game.getPlayer().getLast().getCardsOnHand().size());
				} else if (player == game.getPlayer().getLast()) {
					setOpponentCardCount(game.getPlayer().getFirst().getCardsOnHand().size());
				} else {
					setOpponentCardCount(0);
				}
			}

		}
		return SUCCESS;

		// if (getGameId() != null) {
		// List<Game> gameContainer = (List<Game>) application
		//
		// game = gameContainer.get(Integer.parseInt(getGameId()));
		// player = (Player) session.get("player");
		// setKozr(game.getCardPack().getKozr());
		// setLobby(game.getLobby());
		// setPackSize(game.getCardPack().getPack().size());
		// gameService = new GameService(game);
		// if (game.getStatus() == false &&
		// game.getPlayer().getFirst().getReady()
		// && game.getPlayer().getLast().getReady()) {
		// gameService.startGame();
		// gameService.getGame().getPlayer().getFirst().setTurn(true);
		// System.out.println("starting game N:" + getGameId());
		// }
		// if (getCommand() != null) {
		// if (getCommand().equals("move")) {
		// gameService.move(player, gameService.findAndGetCardById(player,
		// getId()));
		// }
		// if (getCommand().equals("bitta")) {
		// gameService.bitta(player);
		// }
		// if (getCommand().equals("take")) {
		// gameService.take(player);
		// }
		// }
		// }
		// return SUCCESS;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public Card getKozr() {
		return kozr;
	}

	public void setKozr(Card kozr) {
		this.kozr = kozr;
	}

	public LinkedList<Card> getLobby() {
		return lobby;
	}

	public void setLobby(LinkedList<Card> lobby) {
		this.lobby = lobby;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPackSize() {
		return packSize;
	}

	public void setPackSize(int packSize) {
		this.packSize = packSize;
	}

	public int getOpponentCardCount() {
		return opponentCardCount;
	}

	public void setOpponentCardCount(int opponentCardCount) {
		this.opponentCardCount = opponentCardCount;
	}

}
