package am.seroj.action;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;

import am.seroj.service.ChatService;

import com.opensymphony.xwork2.Action;

public class ChatJSONAction implements Action, ApplicationAware, SessionAware {
	
	private String index;
	private String singleMessage;
	private List<String[]> messages;
	private String command;
	private Map<String, Object> session;
	private ChatService chatService;
	private Map<String, Object> application;

	@Override
	public String execute() throws Exception {
		if (application.containsKey("chatService")) {
			chatService = (ChatService) application.get("chatService");
			
			if (getSingleMessage() != null && !StringUtils.isBlank(getSingleMessage())
					&& getCommand().equals("addmessage")) {
				chatService.addMessage((String) session.get("userName"), getSingleMessage());
				messages = chatService.getMessagesByIndex(Integer.parseInt(getIndex()));
			}
			if (getIndex() != null && getCommand().equals("getmessages")) {
				messages = chatService.getMessagesByIndex(Integer.parseInt(getIndex()));
			}
			
		}
		return SUCCESS;
	}
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getSingleMessage() {
		return singleMessage;
	}

	public void setSingleMessage(String singleMessage) {
		this.singleMessage = singleMessage;
	}

	public List<String[]> getMessages() {
		return messages;
	}

	public void setMessages(List<String[]> messages) {
		this.messages = messages;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;		
	}

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;
		
	}

}
