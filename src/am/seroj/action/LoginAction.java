package am.seroj.action;

import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;
import am.seroj.model.Game;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.Preparable;

public class LoginAction implements Action, SessionAware, Preparable, ApplicationAware {
	
	private String userName;
	private Map<String, Object> session;
	private Map<String, Object> application;
	private List<Game> gameContainer;

	@Override
	public String execute() throws Exception {
		if (getUserName() == null) {
			return LOGIN;
		}
		if (session.containsKey("userName") 
				&& session.get("userName").equals(getUserName())) {
			return SUCCESS;
		} else {
			session.put("userName", getUserName());
			return SUCCESS;
		}
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;
	}

	public List<Game> getGameContainer() {
		return gameContainer;
	}

	public void setGameContainer(List<Game> gameContainer) {
		this.gameContainer = gameContainer;
	}
	
	@Override
	public void prepare() throws Exception {
//		if (application.containsKey("gameContainer")){
//			setGameContainer((List<Game>) application.get("gameContainer"));
//		} else {
//			setGameContainer(new ArrayList<Game>());
//			getGameContainer().add(new Game());
//			getGameContainer().add(new Game());
//			getGameContainer().add(new Game());
//			application.put("gameContainer", getGameContainer());
//		}
		setGameContainer((List<Game>) application.get("gameContainer"));
	}

}
