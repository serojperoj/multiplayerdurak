package am.seroj.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.ApplicationAware;
import am.seroj.model.Game;
import com.opensymphony.xwork2.Action;

public class LobbyJSONAction implements Action, ApplicationAware {

	private Map<String, Object> application;
	private int myList[];

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;

	}

	@Override
	public String execute() throws Exception {
		if (application.containsKey("gameContainer")) {
			List<Game> gameContainer = (List<Game>) application.get("gameContainer");
//			setFirst(new Boolean[gameContainer.size()]);
//			setLast(new Boolean[gameContainer.size()]);
			setMyList(new int[gameContainer.size()]);
			for (int i = 0; i < gameContainer.size(); i++) {
//				getFirst()[i] = gameContainer.get(i).getPlayer().getFirst().getReady();
//				getLast()[i] = gameContainer.get(i).getPlayer().getLast().getReady();
				Game game = gameContainer.get(i);
				
				if (!game.getPlayer().getFirst().getWin().equals("") 
						|| !game.getPlayer().getLast().getWin().equals("")) {
					gameContainer.set(i, new Game());
				}
				
				if (game.getPlayer().getFirst().getReady() == true 
						&& game.getPlayer().getLast().getReady() == true) {
					getMyList()[i] = 0;
				} else if (game.getPlayer().getFirst().getReady() == true 
							|| game.getPlayer().getLast().getReady() == true) {
					getMyList()[i] = 1;
				} else {
					getMyList()[i] = 2;
				}
				
//				if (gameContainer.get(i).getPlayer().getFirst().getReady() == true
//						&& gameContainer.get(i).getPlayer().getLast().getReady() == true) {
//					getMyList()[i] = true;
//				} else {
//					getMyList()[i] = false;
//				}
			}
		}
		return SUCCESS;
	}

	// public Boolean[] getLast() {
	// return last;
	// }
	//
	// public void setLast(Boolean last[]) {
	// this.last = last;
	// }
	//
	// public Boolean[] getFirst() {
	// return first;
	// }
	//
	// public void setFirst(Boolean first[]) {
	// this.first = first;
	// }

	public int[] getMyList() {
		return myList;
	}

	public void setMyList(int myList[]) {
		this.myList = myList;
	}

}
