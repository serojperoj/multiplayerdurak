package am.seroj.action;

import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.SessionAware;
import am.seroj.model.Game;
import am.seroj.model.Player;
import com.opensymphony.xwork2.Action;

public class GameAction implements Action, SessionAware, ApplicationAware {

	private String gameId;
	private String playerId;
	private Map<String, Object> application;
	private Map<String, Object> session;
	private List<Game> gameContainer;
	private Game game;

	@Override
	public String execute() throws Exception {
		if (getGameId() != null) {
			gameContainer = (List<Game>) application.get("gameContainer");
			game = gameContainer.get(Integer.parseInt(getGameId()));
			if (session.containsKey("player")) {
				Player player = (Player) session.get("player");
				if ((game.getPlayer().getFirst() != player) && (game.getPlayer().getLast() != player)) {
					player.setReady(false);
					player = null;
					session.remove("player");
//					return INPUT;
				}
			}
			return SUCCESS;
		}
		return LOGIN;
//		gameContainer = (List<Game>) application.get("gameContainer");
//		if (session.containsKey("player") 
//				&& ((session.get("player") != gameContainer.get(Integer.parseInt(getGameId())).getPlayer().getFirst()) 
//				|| (session.get("player") != gameContainer.get(Integer.parseInt(getGameId())).getPlayer().getLast()))) {
//			Player player = (Player) session.get("player");
//			player.setReady(false);
//			player.setName(null);
//			player.getCardsOnHand().clear();
//			player = null;
//		}
//		for (int i = 0; i < gameContainer.size(); i++) {
//			LinkedList<Player> player = gameContainer.get(i).getPlayer();
//			if ((gameContainer.get(i).getStatus() == true)
//					&& (player.getFirst().getReady() == false)
//					&& (player.getLast().getReady()) == false) {
//				gameContainer.set(i, new Game());
//			}
//		}
//		game = gameContainer.get(Integer.parseInt(getGameId()));
//		if ((getPlayerId().equals("first")) && (game.getPlayer().getFirst().getReady() == false)) {
//			Player player = game.getPlayer().getFirst();
//			player.setReady(true);
//			player.setName((String) session.get("userName"));
//			session.put("player", player);
//		} else if ((getPlayerId().equals("last")) && (game.getPlayer().getLast().getReady() == false)) {
//			Player player = game.getPlayer().getLast();
//			player.setReady(true);
//			player.setName((String) session.get("userName"));
//			session.put("player", player);
//		}
		
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;

	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

}
