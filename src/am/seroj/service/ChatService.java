package am.seroj.service;

import java.util.LinkedList;
import java.util.List;

public class ChatService {

	private List<String[]> messages;
	private List<String[]> mess;
	
	
	public ChatService() {
		setMessages(new LinkedList<String[]>());
		mess = new LinkedList<String[]>();
	}
	
	public List<String[]> getMessagesByIndex(int index) {
		mess.clear();
		for (int i = index; i < getMessages().size(); i++) {
			mess.add(getMessages().get(i));
		}
		return mess;
	}
	
	public void addMessage(String userName, String message) {
		String[] array = new String[2];
		array[0] = userName;
		array[1] = message;
		getMessages().add(array);
	}

	public List<String[]> getMessages() {
		return messages;
	}

	public void setMessages(List<String[]> messages) {
		this.messages = messages;
	}
}
