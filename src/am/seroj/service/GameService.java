package am.seroj.service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

import am.seroj.model.Card;
import am.seroj.model.CardPack;
import am.seroj.model.Game;
import am.seroj.model.Player;

public class GameService {

	private Game game;

	public GameService(Game game) {
		this.setGame(game);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void startGame() {
		randomize(game.getCardPack());
		deal();
		getGame().setStatus(true);
	}

	private void randomize(CardPack cardPack) {
		Collections.shuffle(cardPack.getPack());
//		Random rand = new Random();
//		for (int i = 0; i < 500; i++) {
//			int start = rand.nextInt(35);
//			int end = rand.nextInt(35);
//			Card item = cardPack.getPack().get(start);
//			cardPack.getPack().set(start, cardPack.getPack().get(end));
//			cardPack.getPack().set(end, item);
//		}
		cardPack.setKozr(cardPack.getPack().getFirst());
	}

	private void deal() {
		if (game.getPlayer().getFirst().getTurn() == true) {
			while (game.getPlayer().getFirst().getCardsOnHand().size() < 6
					&& game.getCardPack().getPack().size() > 0) {
				game.getPlayer().getFirst().getCardsOnHand()
						.addLast(game.getCardPack().getPack().removeLast());
			}
			while (game.getPlayer().getLast().getCardsOnHand().size() < 6
					&& game.getCardPack().getPack().size() > 0) {
				game.getPlayer().getLast().getCardsOnHand()
						.addLast(game.getCardPack().getPack().removeLast());
			}
		} else {
			while (game.getPlayer().getLast().getCardsOnHand().size() < 6
					&& game.getCardPack().getPack().size() > 0) {
				game.getPlayer().getLast().getCardsOnHand()
						.addLast(game.getCardPack().getPack().removeLast());
			}
			while (game.getPlayer().getFirst().getCardsOnHand().size() < 6
					&& game.getCardPack().getPack().size() > 0) {
				game.getPlayer().getFirst().getCardsOnHand()
						.addLast(game.getCardPack().getPack().removeLast());
			}
		}
		
	}

	public void move(Player player, Card card) {
		if (player.getTurn()) {
			if (game.getGiveMoreVariable() == true) {
				if (checkSimilarCards(card)) {
					int index = player.getCardsOnHand().indexOf(card);
					game.getLobby().add(player.getCardsOnHand().get(index));
					player.getCardsOnHand().remove(index);
				} else {
					return;
				}
			} else if (game.getLobby().size() % 2 == 0) {
				if (game.getLobby().size() != 0 && !checkSimilarCards(card)) {
					return;
				}
				int index = player.getCardsOnHand().indexOf(card);
				game.getLobby().add(player.getCardsOnHand().get(index));
				player.getCardsOnHand().remove(index);
				switchTurn();
			} else {
				if (check(card)) {
					int index = player.getCardsOnHand().indexOf(card);
					game.getLobby().add(player.getCardsOnHand().get(index));
					player.getCardsOnHand().remove(index);
					switchTurn();
				}
			}
		} 
		decideWinner();
	}

	private void switchTurn() {
		if (game.getPlayer().getFirst().getTurn() == true) {
			game.getPlayer().getFirst().setTurn(false);
			game.getPlayer().getLast().setTurn(true);
		} else {
			game.getPlayer().getFirst().setTurn(true);
			game.getPlayer().getLast().setTurn(false);
		}
	}

	private Boolean checkSimilarCards(Card card) {
		for (Card c : game.getLobby()) {
			if (c.getValue() == card.getValue()) {
				return true;
			}
		}
		return false;
	}

	private Boolean check(Card card) {
		if (card.getMast().equals(game.getLobby().getLast().getMast())
				&& card.getValue() > game.getLobby().getLast().getValue()) {
			return true;
		} else if (card.getMast()
				.equals(game.getCardPack().getKozr().getMast())
				&& !game.getLobby().getLast().getMast()
						.equals(game.getCardPack().getKozr().getMast())) {
			return true;
		}
		return false;

	}

	public void bitta(Player player) {
		if (player.getTurn() && game.getLobby().size() % 2 == 0) {
			game.getLobby().clear();
			deal();
//			decideWinner();
			switchTurn();
		}
		
	}

	public void take(Player player) {
		if (player.getTurn()) {
			if (game.getGiveMoreVariable() == false) {
				if (game.getLobby().size() % 2 != 0) {
					game.setGiveMoreVariable(true);
					if (player == game.getPlayer().getFirst()) {
						game.getPlayer().getLast().setMessage(
								"I am taking cards, do you have anymore? After it all, click Take button.");
					} else {
						game.getPlayer().getFirst().setMessage(
								"I am taking cards, do you have anymore? After it all, click Take button.");
					}
					
					switchTurn();
					return;
				}
			} else {
				if (player == game.getPlayer().getFirst()) {
					while (game.getLobby().size() > 0) {
						game.getPlayer().getLast().getCardsOnHand().addLast(game.getLobby().removeLast());
					}
				} else {
					while (game.getLobby().size() > 0) {
						game.getPlayer().getFirst().getCardsOnHand().addLast(game.getLobby().removeLast());
					}
				}
				game.getPlayer().getFirst().setMessage("");
				game.getPlayer().getLast().setMessage("");
				game.setGiveMoreVariable(false);
				deal();
//				decideWinner();
			}	
		}
		
	}

	public Card findAndGetCardById(Player player, String id) {
		if (player.getTurn()) {
			for (Card c : player.getCardsOnHand()) {
				if (c.getPath().equals(id)) {
					return c;
				}
			}
		}
		return null;
	}

	private void decideWinner() {
		LinkedList<Player> pl = game.getPlayer();
		if (pl.getFirst().getCardsOnHand().isEmpty() 
				&& pl.getLast().getCardsOnHand().size() > 1 
				&& game.getCardPack().getPack().isEmpty()) {
			pl.getFirst().setWin("win");
			pl.getLast().setWin("loss");
//			game.setStatus(false);
		} else if (pl.getLast().getCardsOnHand().isEmpty() 
				&& pl.getFirst().getCardsOnHand().size() > 1
				&& game.getCardPack().getPack().isEmpty()) {
			pl.getLast().setWin("win");
			pl.getFirst().setWin("loss");
//			game.setStatus(false);
			
		} else if (pl.getFirst().getCardsOnHand().isEmpty() 
				&& pl.getLast().getCardsOnHand().isEmpty() 
				&& game.getCardPack().getPack().isEmpty()) {
			pl.getFirst().setWin("nichya");
			pl.getLast().setWin("nichya");
//			game.setStatus(false);
			
		}

	}
}
